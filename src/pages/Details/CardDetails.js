import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import noImgAvailable from "./No-Image-Placeholder.svg.png";
import { SkeletonDetails } from "../../components/Skeleton/SkeletonDetails";
import { options } from "../../constants";
import "./CardDetails.scss";

export default function CardDetails({ setShowDropdown, setShowBorder }) {
  const [game, setGame] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { id } = useParams();

  const {
    title,
    thumbnail,
    status,
    description,
    genre,
    platform,
    publisher,
    developer,
    release_date,
    screenshots,
  } = game;

  const api = `https://free-to-play-games-database.p.rapidapi.com/api/game?id=${id}`;

  useEffect(() => {
    setShowDropdown(false);
    setShowBorder(false);
    fetch(api, options)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setGame(data);
        setIsLoading(false);
      });

    return () => {
      setShowDropdown(true);
      setShowBorder(true);
    };
  }, [id, api, setShowDropdown, setShowBorder]);

  return (
    <>
      {isLoading ? (
        <div className="skeleton-details">
          <SkeletonDetails />
        </div>
      ) : (
        <div className={status === 0 ? "display-none" : "card-details"}>
          <div className="images">
            {thumbnail && screenshots.length !== 0 ? (
              <>
                <img src={thumbnail} alt="Thumbnail" />

                {screenshots?.map((screenshot) => {
                  return (
                    <img
                      src={screenshot.image}
                      key={screenshot.id}
                      alt="Screenshots"
                    />
                  );
                })}
              </>
            ) : (
              <img src={noImgAvailable} alt="NoImage" />
            )}
          </div>
          <div className="text">
            <h1>
              {title} - {release_date}
            </h1>
            <div className="flex">
              <span className={status === "Live" ? "online" : "offline"}>
                {status}
              </span>
              <span>{genre}</span>
            </div>
            <p>{description}</p>
            <p>{platform}</p>
            <h2>
              {publisher} - {developer}
            </h2>
          </div>
        </div>
      )}
      {status === 0 ? <p className="notfound">No game found!</p> : ""}
    </>
  );
}
