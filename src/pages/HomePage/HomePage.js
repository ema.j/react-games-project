import React, { useEffect, useState } from "react";
import CardList from "../../components/Card List/CardList";
import { SkeletonCard } from "../../components/Skeleton/SkeletonCard";
import { skeletonCards } from "../../constants/index";
import { options } from "../../constants/index";
import "../../components/Skeleton/Skeleton.scss";

export default function HomePage({ sort, category }) {
  const [games, setGames] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const api = `https://free-to-play-games-database.p.rapidapi.com/api/games?sort-by=${sort}`;
  const api2 = `https://free-to-play-games-database.p.rapidapi.com/api/games?category=${category}&sort-by=${sort}`;

  // const options = {
  //   method: "GET",
  //   headers: {
  //     "X-RapidAPI-Key": "ae4ecdd51amsh3b6e5ab8e31ab85p188599jsna30ed12bed19",
  //     "X-RapidAPI-Host": "free-to-play-games-database.p.rapidapi.com",
  //   },
  // };

  useEffect(() => {
    if (category && category !== "all") {
      fetch(api2, options)
        .then((res) => {
          setIsLoading(true);
          return res.json();
        })
        .then((data) => {
          setGames(data);
          setIsLoading(false);
        });
    } else {
      fetch(api, options)
        .then((res) => {
          setIsLoading(true);
          return res.json();
        })
        .then((data) => {
          setGames(data);
          setIsLoading(false);
        });
    }
  }, [sort, category, api, api2]);

  return (
    <>
      {isLoading ? (
        <div className="skeleton-cards">
          {skeletonCards?.map((game, index) => (
            <SkeletonCard key={index} />
          ))}
        </div>
      ) : (
        <CardList games={games} />
      )}
    </>
  );
}
