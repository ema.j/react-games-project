import React from "react";
import Card from "../Card/Card";
import "./CardList.scss";

export default function CardList({ games }) {
  return (
    <ul className="game-list">
      {games?.map((game) => (
        <Card
          key={game.id}
          id={game.id}
          thumbnail={game.thumbnail}
          title={game.title}
          description={game.short_description}
          genre={game.genre}
        />
      ))}
    </ul>
  );
}
