import React from "react";
import "./Skeleton.scss";

export const SkeletonCard = () => {
  return (
    <div className="skeleton-wrapper">
      <div className="skeleton-card">
        <div className="skeleton-thumbnail" />
        <div className="skeleton-title" />
        <div className="skeleton-text" />
        <div className="flex">
          <div className="skeleton-genre" />
          <div className="skeleton-button" />
        </div>
      </div>
    </div>
  );
};
