import React from "react";
import "./Skeleton.scss";

export const SkeletonDetails = () => {
  return (
    <>
      <div className="skeleton-images">
        <div className="skeleton-image" />
        <div className="skeleton-image" />
        <div className="skeleton-image" />
        <div className="skeleton-image" />
      </div>
      <div className="skeleton-content">
        <div className="skeleton-title-date" />
        <div className="flex">
          <div className="skeleton-status" />
          <div className="skeleton-genre" />
        </div>
        <div className="skeleton-desc" />
        <div className="skeleton-platform" />
        <div className="skeleton-publisher-dev" />
      </div>
    </>
  );
};
