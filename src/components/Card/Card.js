import React from "react";
import { Link } from "react-router-dom";
import "./Card.scss";

export default function Card({ id, thumbnail, title, description, genre }) {
  return (
    <div className="card">
      <div className="img">
        <img src={thumbnail} alt="game" />
      </div>
      <div className="content">
        <h2 className="title">{title}</h2>
        <p className="desc">{description}</p>
        <div className="wrapper">
          <span className="genre">{genre}</span>
          <Link to={`${id}`}>
            <button>Learn More +</button>
          </Link>
        </div>
      </div>
    </div>
  );
}
