import React from "react";
import "./DropDown.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

export default function DropDown({ category, setCategory }) {
  const categories = ["all", "mmorpg", "shooter", "pvp", "mmofps", "strategy"];

  const handleCategory = (category) => {
    setCategory(category);
  };

  return (
    <div className="dropdown">
      <button className="btn">
        {category ? category : "Select Category"}
        <FontAwesomeIcon icon={faChevronDown} />
      </button>
      <ul className="dropdown-content">
        {categories.map((category, i) => (
          <li
            className="dropdown-el"
            onClick={() => handleCategory(category)}
            key={i}
          >
            {category}
          </li>
        ))}
      </ul>
    </div>
  );
}
