import React from "react";
import { Link } from "react-router-dom";
import DropDown from "./DropDown";
import logo from "./freetogame-logo.png";
import classnames from "classnames";
import "./Navbar.scss";

export default function Navbar({
  sort,
  setSort,
  category,
  setCategory,
  showDropdown,
  showBorder,
}) {
  const handleTab = (tab) => {
    setSort(tab);
  };

  return (
    <nav className="nav">
      <>
        <Link to="/" style={{ textDecoration: "none" }}>
          <div className="logo">
            <img src={logo} alt="Logo" />
          </div>
        </Link>
        <ul className="list">
          <Link to="/" style={{ textDecoration: "none" }}>
            <li
              className={classnames({
                selected: sort === "alphabetical",
                "not-selected": !showBorder,
              })}
              onClick={() => handleTab("alphabetical")}
            >
              Alphabetical
            </li>
          </Link>
          <Link to="/" style={{ textDecoration: "none" }}>
            <li
              className={classnames({
                selected: sort === "relevance",
                "not-selected": !showBorder,
              })}
              onClick={() => handleTab("relevance")}
            >
              Relevance
            </li>
          </Link>
          <Link to="/" style={{ textDecoration: "none" }}>
            <li
              className={classnames({
                selected: sort === "release-date",
                "not-selected": !showBorder,
              })}
              onClick={() => handleTab("release-date")}
            >
              Release Date
            </li>
          </Link>
          <Link to="/" style={{ textDecoration: "none" }}>
            <li
              className={classnames({
                selected: sort === "popularity",
                "not-selected": !showBorder,
              })}
              onClick={() => handleTab("popularity")}
            >
              Popularity
            </li>
          </Link>
        </ul>
        {showDropdown && (
          <DropDown sort={sort} category={category} setCategory={setCategory} />
        )}
      </>
    </nav>
  );
}
