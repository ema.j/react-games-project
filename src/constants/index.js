export const skeletonCards = Array(60).fill();

export const options = {
  method: "GET",
  headers: {
    "X-RapidAPI-Key": "ae4ecdd51amsh3b6e5ab8e31ab85p188599jsna30ed12bed19",
    "X-RapidAPI-Host": "free-to-play-games-database.p.rapidapi.com",
  },
};
