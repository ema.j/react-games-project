import React, { useState } from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import CardDetails from "./pages/Details/CardDetails";
import Navbar from "./components/Navbar/Navbar";

function App() {
  const [sort, setSort] = useState("alphabetical");
  const [category, setCategory] = useState();
  const [showDropdown, setShowDropdown] = useState(true);
  const [showBorder, setShowBorder] = useState(true);

  return (
    <>
      <Navbar
        sort={sort}
        setSort={setSort}
        category={category}
        setCategory={setCategory}
        showDropdown={showDropdown}
        showBorder={showBorder}
      />
      <Routes>
        <Route
          path="/"
          element={<HomePage sort={sort} category={category} />}
        />
        <Route
          path="/:id"
          element={
            <CardDetails
              setShowDropdown={setShowDropdown}
              setShowBorder={setShowBorder}
            />
          }
        />
      </Routes>
    </>
  );
}

export default App;
